package edu.logica1;

import edu.cableado.Comunicacion;
import edu.cableado.Despacho;
import edu.cableado.Inventariable;
import edu.cableado.Publicida;
import edu.cableado.Recaudo;
import edu.utilitarios.Cargador;

public class FingerChain {

	public static void main(String[] args) {
		Cargador cf = new Cargador("front", ClassLoader.getSystemClassLoader());
		Cargador cb = new Cargador("back", ClassLoader.getSystemClassLoader());
		Class cls;
		
		try {
			 cls = cf.cargarUnaClaseDesdeSuDirectorio(Peticion.class.getName());
			 if(cls!=null) {
				 Peticion ic=(Peticion)cls.getConstructor().newInstance();
				 ic.captarHuella();
			 }
			 cls = cf.cargarUnaClaseDesdeSuDirectorio(Registro.class.getName());
			 if(cls!=null) {
				 Registro id=(Registro)cls.getConstructor().newInstance();
				 id.registrarUsuario();
			 }
			 cls = cf.cargarUnaClaseDesdeSuDirectorio(Acceso.class.getName());
			 if(cls!=null) {
				 Acceso ip=(Acceso)cls.getConstructor().newInstance();
				 ip.acceso();
				 ip.restriccion();
			 }
			 cls = cb.cargarUnaClaseDesdeSuDirectorio(Autenticacion.class.getName());
			 if(cls!=null) {
				 Autenticacion ir=(Autenticacion)cls.getConstructor().newInstance();
				 ir.procesoAutenticacion();
			 }
		}catch(Exception e) {e.printStackTrace();}

	}

}