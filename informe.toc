\babel@toc {spanish}{}
\contentsline {part}{I\hspace {1em}PROYECTO}{11}
\contentsline {chapter}{\numberline {1}Anteproyecto}{13}
\contentsline {section}{\numberline {1.1}Introducci\IeC {\'o}n}{13}
\contentsline {section}{\numberline {1.2}T\IeC {\'\i }tulo y definici\IeC {\'o}n del problema de investigaci\IeC {\'o}n}{15}
\contentsline {section}{\numberline {1.3}Estudio del problema de investigaci\IeC {\'o}n}{15}
\contentsline {subsection}{\numberline {1.3.1}Planteamiento del problema}{15}
\contentsline {subsection}{\numberline {1.3.2}Formulaci\IeC {\'o}n del problema}{16}
\contentsline {subsection}{\numberline {1.3.3}Sistematizaci\IeC {\'o}n del problema}{16}
\contentsline {section}{\numberline {1.4}Objetivos de la investigaci\IeC {\'o}n}{17}
\contentsline {subsection}{\numberline {1.4.1}Objetivo general}{17}
\contentsline {subsection}{\numberline {1.4.2}Objetivos Espec\IeC {\'\i }ficos}{17}
\contentsline {section}{\numberline {1.5}Justificaci\IeC {\'o}n de la investigaci\IeC {\'o}n}{18}
\contentsline {subsection}{\numberline {1.5.1}Justificaci\IeC {\'o}n Pr\IeC {\'a}ctica}{18}
\contentsline {section}{\numberline {1.6}Hip\IeC {\'o}tesis del trabajo}{18}
\contentsline {section}{\numberline {1.7}Marco referencial}{19}
\contentsline {subsection}{\numberline {1.7.1}Marco te\IeC {\'o}rico}{19}
\contentsline {subsubsection}{Procesamiento Digital de Im\IeC {\'a}genes}{19}
\contentsline {subsubsection}{Detecci\IeC {\'o}n de rostros}{19}
\contentsline {subsubsection}{Reconocimiento facial (Patrones Binarios Locales)}{20}
\contentsline {subsubsection}{Biometr\IeC {\'\i }a}{20}
\contentsline {subsubsection}{Blockchain}{21}
\contentsline {subsubsection}{Principios del blockchain}{22}
\contentsline {subsubsection}{Sistemas centralizados vs Sistemas descentralizados}{23}
\contentsline {subsection}{\numberline {1.7.2}Marco conceptual}{23}
\contentsline {subsubsection}{Reconocimiento Facial con OpenCV}{23}
\contentsline {subsubsection}{Sistemas embebidos}{24}
\contentsline {section}{\numberline {1.8}Aspectos metodol\IeC {\'o}gicos}{25}
\contentsline {subsection}{\numberline {1.8.1}Tipo de estudio}{25}
\contentsline {subsection}{\numberline {1.8.2}M\IeC {\'e}todo de investigaci\IeC {\'o}n}{25}
\contentsline {subsection}{\numberline {1.8.3}Fuentes y t\IeC {\'e}cnicas para la recolecci\IeC {\'o}n de datos}{25}
\contentsline {subsubsection}{Fuentes primarias}{25}
\contentsline {subsubsection}{Fuentes te\IeC {\'o}ricas}{26}
\contentsline {section}{\numberline {1.9}Tratamiento de la informaci\IeC {\'o}n}{27}
\contentsline {section}{\numberline {1.10}Alcances, limitaciones y resultados esperados}{28}
\contentsline {subsection}{\numberline {1.10.1}Alcances}{28}
\contentsline {subsection}{\numberline {1.10.2}Limitaciones}{28}
\contentsline {subsection}{\numberline {1.10.3}Resultados Esperados}{28}
\contentsline {section}{\numberline {1.11}Tabla de contenidos del proyecto}{29}
\contentsline {subsection}{\numberline {1.11.1}CAP\IeC {\'I}TULO 1. CONTEXTUALIZACION DE LA INVESTIGACI\IeC {\'O}N}{29}
\contentsline {subsection}{\numberline {1.11.2}CAP\IeC {\'I}TULO 2. DESARROLLO DE LA INVESTIGACI\IeC {\'O}N}{29}
\contentsline {subsection}{\numberline {1.11.3}CAP\IeC {\'I}TULO 3. CIERRE DE LA INVESTIGACI\IeC {\'O}N}{29}
\contentsline {chapter}{\numberline {2}Metodolog\IeC {\'\i }a}{33}
\contentsline {section}{\numberline {2.1}Introducci\IeC {\'o}n}{33}
\contentsline {section}{\numberline {2.2}Scrum}{34}
\contentsline {subsection}{\numberline {2.2.1}Primer sprint: Blockchain }{35}
\contentsline {subsection}{\numberline {2.2.2}Primera semana OFF}{35}
\contentsline {subsection}{\numberline {2.2.3}Segundo sprint": Implementaci\IeC {\'o}n acceso por reconocimiento facial}{35}
\contentsline {subsection}{\numberline {2.2.4}Segunda semana OFF}{35}
\contentsline {subsection}{\numberline {2.2.5}Tercer sprint: Integraci\IeC {\'o}n blockchain - prototipo reconocimiento facial. }{35}
\contentsline {subsection}{\numberline {2.2.6}Tercera semana OFF}{36}
\contentsline {part}{II\hspace {1em}ARQUITECTURA Y DISE\IeC {\~N}O}{37}
\contentsline {chapter}{\numberline {3}Organizaci\IeC {\'o}n}{39}
\contentsline {section}{\numberline {3.1}Introducci\IeC {\'o}n}{39}
\contentsline {section}{\numberline {3.2}Nombre de la Organizaci\IeC {\'o}n: Teranov}{40}
\contentsline {section}{\numberline {3.3}Misi\IeC {\'o}n}{40}
\contentsline {section}{\numberline {3.4}Visi\IeC {\'o}n}{40}
\contentsline {section}{\numberline {3.5}Objetivos Organizacionales}{40}
\contentsline {section}{\numberline {3.6}Organigrama}{42}
\contentsline {section}{\numberline {3.7}Manual de Funciones}{43}
\contentsline {section}{\numberline {3.8}Servicios y Productos}{46}
\contentsline {chapter}{\numberline {4}Archimate y ADM}{47}
\contentsline {section}{\numberline {4.1}Introducci\IeC {\'o}n}{47}
\contentsline {section}{\numberline {4.2}Conceptos generales de Archimate}{48}
\contentsline {section}{\numberline {4.3}Tabla de Negocio}{49}
\contentsline {section}{\numberline {4.4}Tabla de Aplicaci\IeC {\'o}n}{51}
\contentsline {section}{\numberline {4.5}Tabla de Tecnolog\IeC {\'\i }a}{52}
\contentsline {section}{\numberline {4.6}Tabla de Relaciones}{54}
\contentsline {section}{\numberline {4.7}Tabla de Motivaci\IeC {\'o}n Migraci\IeC {\'o}n}{56}
\contentsline {section}{\numberline {4.8}ADM}{57}
\contentsline {chapter}{\numberline {5}Negocio}{59}
\contentsline {section}{\numberline {5.1}Introducci\IeC {\'o}n}{59}
\contentsline {section}{\numberline {5.2}Punto de Vista de Organizaci\IeC {\'o}n}{60}
\contentsline {subsection}{\numberline {5.2.1}Modelo}{60}
\contentsline {subsection}{\numberline {5.2.2}Caso}{61}
\contentsline {section}{\numberline {5.3}Punto de Vista de Cooperaci\IeC {\'o}n de Actor}{62}
\contentsline {subsection}{\numberline {5.3.1}Modelo}{62}
\contentsline {subsection}{\numberline {5.3.2}Caso}{63}
\contentsline {section}{\numberline {5.4}Punto de Vista de Funci\IeC {\'o}n de Negocio}{64}
\contentsline {subsection}{\numberline {5.4.1}Modelo}{64}
\contentsline {subsection}{\numberline {5.4.2}Caso}{65}
\contentsline {section}{\numberline {5.5}Punto de Vista de Proceso de Negocio}{66}
\contentsline {subsection}{\numberline {5.5.1}Modelo}{66}
\contentsline {subsection}{\numberline {5.5.2}Caso}{67}
\contentsline {section}{\numberline {5.6}Punto de Vista de Cooperaci\IeC {\'o}n de Proceso de Negocio}{68}
\contentsline {subsection}{\numberline {5.6.1}Modelo}{68}
\contentsline {subsection}{\numberline {5.6.2}Caso}{69}
\contentsline {section}{\numberline {5.7}Punto de Vista de Producto}{70}
\contentsline {subsection}{\numberline {5.7.1}Modelo}{70}
\contentsline {subsection}{\numberline {5.7.2}Caso}{71}
\contentsline {chapter}{\numberline {6}Software}{73}
\contentsline {section}{\numberline {6.1}Introducci\IeC {\'o}n}{73}
\contentsline {section}{\numberline {6.2}Punto de Vista de Comportamiento de Aplicaci\IeC {\'o}n}{74}
\contentsline {subsection}{\numberline {6.2.1}Modelo}{74}
\contentsline {subsection}{\numberline {6.2.2}Caso}{75}
\contentsline {section}{\numberline {6.3}Punto de Vista de Cooperaci\IeC {\'o}n de Aplicaci\IeC {\'o}n}{76}
\contentsline {subsection}{\numberline {6.3.1}Modelo}{76}
\contentsline {subsection}{\numberline {6.3.2}Caso}{77}
\contentsline {section}{\numberline {6.4}Punto de Vista de Estructura de Aplicaci\IeC {\'o}n}{78}
\contentsline {subsection}{\numberline {6.4.1}Modelo}{78}
\contentsline {subsection}{\numberline {6.4.2}Caso}{79}
\contentsline {subsection}{\numberline {6.4.3}Codigo Fuente}{80}
\contentsline {section}{\numberline {6.5}Punto de Vista de Uso de Aplicaci\IeC {\'o}n}{82}
\contentsline {subsection}{\numberline {6.5.1}Modelo}{82}
\contentsline {subsection}{\numberline {6.5.2}Caso}{83}
\contentsline {chapter}{\numberline {7}Tecnolog\IeC {\'\i }a}{85}
\contentsline {section}{\numberline {7.1}Introducci\IeC {\'o}n}{85}
\contentsline {section}{\numberline {7.2}Punto de Vista de Infraestructura}{86}
\contentsline {subsection}{\numberline {7.2.1}Modelo}{86}
\contentsline {subsection}{\numberline {7.2.2}Caso}{87}
\contentsline {section}{\numberline {7.3}Punto de Vista de Uso de Infraestructura}{88}
\contentsline {subsection}{\numberline {7.3.1}Modelo}{88}
\contentsline {subsection}{\numberline {7.3.2}Caso}{89}
\contentsline {section}{\numberline {7.4}Punto de Vista de Implementaci\IeC {\'o}n y Despliegue}{90}
\contentsline {subsection}{\numberline {7.4.1}Modelo}{90}
\contentsline {subsection}{\numberline {7.4.2}Caso}{91}
\contentsline {section}{\numberline {7.5}Punto de Vista de Estructura de la Informaci\IeC {\'o}n}{92}
\contentsline {subsection}{\numberline {7.5.1}Modelo}{92}
\contentsline {subsection}{\numberline {7.5.2}Caso}{93}
\contentsline {section}{\numberline {7.6}Punto de Vista de Cooperaci\IeC {\'o}n de Realizaci\IeC {\'o}n del Servicio}{94}
\contentsline {subsection}{\numberline {7.6.1}Modelo}{94}
\contentsline {subsection}{\numberline {7.6.2}Caso}{95}
\contentsline {section}{\numberline {7.7}Punto de Vista de Capas}{96}
\contentsline {subsection}{\numberline {7.7.1}Modelo}{96}
\contentsline {subsection}{\numberline {7.7.2}Caso}{97}
\contentsline {chapter}{\numberline {8}Motivaci\IeC {\'o}n}{99}
\contentsline {section}{\numberline {8.1}Introducci\IeC {\'o}n}{99}
\contentsline {section}{\numberline {8.2}Punto de Vista de Stakeholders}{100}
\contentsline {subsection}{\numberline {8.2.1}Modelo}{100}
\contentsline {subsection}{\numberline {8.2.2}Caso}{101}
\contentsline {section}{\numberline {8.3}Punto de Vista de Realizaci\IeC {\'o}n de Objetivos}{102}
\contentsline {subsection}{\numberline {8.3.1}Modelo}{102}
\contentsline {subsection}{\numberline {8.3.2}Caso}{103}
\contentsline {section}{\numberline {8.4}Punto de Vista de contribuci\IeC {\'o}n de Objetivos}{104}
\contentsline {subsection}{\numberline {8.4.1}Modelo}{104}
\contentsline {subsection}{\numberline {8.4.2}Caso}{105}
\contentsline {section}{\numberline {8.5}Punto de Vista de Realizaci\IeC {\'o}n de Requerimientos}{106}
\contentsline {subsection}{\numberline {8.5.1}Modelo}{106}
\contentsline {subsection}{\numberline {8.5.2}Caso}{107}
\contentsline {section}{\numberline {8.6}Punto de Vista de Principios}{108}
\contentsline {subsection}{\numberline {8.6.1}Modelo}{108}
\contentsline {subsection}{\numberline {8.6.2}Caso}{109}
\contentsline {section}{\numberline {8.7}Punto de Vista de Motivaci\IeC {\'o}n}{110}
\contentsline {subsection}{\numberline {8.7.1}Modelo}{110}
\contentsline {subsection}{\numberline {8.7.2}Caso}{111}
\contentsline {chapter}{\numberline {9}Migraci\IeC {\'o}n}{113}
\contentsline {section}{\numberline {9.1}Introducci\IeC {\'o}n}{113}
\contentsline {section}{\numberline {9.2}Punto de Vista de Proyecto}{114}
\contentsline {subsection}{\numberline {9.2.1}Modelo}{114}
\contentsline {subsection}{\numberline {9.2.2}Caso}{115}
\contentsline {section}{\numberline {9.3}Punto de Vista de Migraci\IeC {\'o}n}{116}
\contentsline {subsection}{\numberline {9.3.1}Modelo}{116}
\contentsline {subsection}{\numberline {9.3.2}Caso}{117}
\contentsline {section}{\numberline {9.4}Punto de Vista de Implementaci\IeC {\'o}n y Migraci\IeC {\'o}n}{118}
\contentsline {subsection}{\numberline {9.4.1}Modelo}{118}
\contentsline {subsection}{\numberline {9.4.2}Caso}{119}
\contentsline {chapter}{\numberline {10}Patrones}{121}
\contentsline {section}{\numberline {10.1}Introducci\IeC {\'o}n}{121}
\contentsline {chapter}{\numberline {11}Creacionales}{123}
\contentsline {section}{\numberline {11.1}Introducci\IeC {\'o}n}{123}
\contentsline {section}{\numberline {11.2}Singleton}{124}
\contentsline {subsection}{\numberline {11.2.1}Modelo Caso de Uso}{124}
\contentsline {subsection}{\numberline {11.2.2}Moldeo Secuencia}{124}
\contentsline {subsection}{\numberline {11.2.3}Modelo de Clases}{124}
\contentsline {subsection}{\numberline {11.2.4}Caso Caso de Uso}{124}
\contentsline {subsection}{\numberline {11.2.5}Caso Secuencia}{124}
\contentsline {subsection}{\numberline {11.2.6}Caso de Clases}{124}
\contentsline {chapter}{\numberline {12}Estructurales}{127}
\contentsline {section}{\numberline {12.1}Introducci\IeC {\'o}n}{127}
\contentsline {chapter}{\numberline {13}Comportamiento}{129}
\contentsline {section}{\numberline {13.1}Introducci\IeC {\'o}n}{129}
\contentsline {part}{III\hspace {1em}CONSIDERACIONES FINALES}{131}
